## Prueba técnica para Liveclicker
Autor: Matias Peyroti

## Instrucciones
Para correr el server primero colocar el archivo dev.env en la carpeta ***api*** y luego ejecutar:
```javascript
yarn setup
yarn start
```
con el script start alcanza para correr la aplicacion. El script prestart se encarga de la instalacion de las librerias, transpilado  y linkeado *common* para que pueda ser usado por *api*.
Para correr los test unitarios ejecutar:
```javascript
yarn start
```

### Descripcion general
El proyecto esta compuesto de 3 paquetes codificados principalmente en typescript:*api*, *common* y *client*. Solo api y *common* están completados.
El *api* contiene el backend que expone la api REST. La idea de *client* es que contenga la aplicacion frontend, y el *common* reune interfaces y algunas funcione utilizadas por api y client.
El archivo src/endpoints.spec.ts contiene algunos test unitarios (no ofrecen un testeo exaustivo pero la covertura de codigo es alta)
Parte del comportamiento de la aplicacion de puede parametrizar mediante variables de entorno. Por ejemplo, las credenciales utilizadas para autenticarse con el servicio S3 de AWS, el base path de los endpoints, o el secret utilizado para cifrar el web token . El archivo dev.env contiene las variables de entorno y sus correspondientes valores que serán seteadas en ambiente de desarrollo.

#### Descripcion de modulo api
El paquete *api* corre una aplicacionó express y realiza el ruteo de acuerdo a la especificación presente en el archivo *openapi.yaml*.
La librería que se encarga de esto es **swagger-tools**, y las funciones que manejan los requests recibidos se encuentran el la carpeta src/controllers. Cada una de estas funciones utilizan la funcionalidad provista por las classes en src/models para ejecutar las operaciones solicitadas.
El mapeo entre los endpoints y las funciones controller se especifica en openapi.yaml con las claves **x-swagger-router-controller** (nombre del controlador) y **operationId** (nombre de la operacion) y se instrumenta con el middleware **swaggerRouter**.

Por ejemplo la siguiente especificacion indica que un POST a /profiles/images debe ser atendido por la funcion profileImagesPost del controller Profile...
```yaml
/profile/images:
    post:
      x-swagger-router-controller: Profile
      operationId: profileImagesPost
...
```
y con el siguiente codóigo le indico al middleware swaggerRouter cual es la funcionó que debe manejar los requests...
```javascript
const controllersObject = {
  Profiles_profileImagesPost: (req, res) => {...}
}
app.use(middleware.swaggerRouter({
  controllers: controllersObject
}))
```

Para mas información sobre las opearaciones ver el archivo [openapi.yaml](./common/openapi.yaml)

#### Descripcion de modulo common
Este modulo contiene algunas interfaces y tipos usados por *api*, por ejemplo la interfaz ```UserInfo``` utilizada por la clase [api/src/models/Security](./api/src/models/Security.ts). 
```typescript
export interface UserInfo{
  userName: string
  firstName: string
  lastName: string
  countryCode: string
}
```

Common también contiene clases que se pueden utilizar como clientes para utilizar la api REST. Las clases fueron generadas automaticamente con swagger-codegen en base a la especificacion presente en common/openapi.yaml
Para generar el código se debe intalar swagger-codegen 2.4.12 y correr el siguiente script desde la carpeta common:
```javascript
yarn gen-api
```

El archivo [testRequests.http](./testRequests.http) contiene un par de ejemplos de request que el servicio es capaz de manejar. 