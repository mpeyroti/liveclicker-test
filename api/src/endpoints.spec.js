const request = require('supertest')
const {app, httpServer} = require('server')
const Jimp = require('jimp')
const fs = require('fs')
const TEST_IMAGES_DIR = './test'

afterAll(() => {
  httpServer.close()
})

describe('Login endpoint', () => {
  it('Succesfull login', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: 'usuario',
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(200)
    .then(res => {
      expect(res.body.result).toEqual('success')
      expect(res.body).toHaveProperty('token')
      expect(res.body.token.length).toBeGreaterThan(0)
    })
  })
  it('Login fail', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: 'invalidUser',
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(401)
    .then(res => {
      expect(res.body.result).toEqual('invalidUserOrPassword')
    })
  })
  it('Login fail', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: Array(50).fill('a').join(''),
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(401)
    .then(res => {
      expect(res.body.result).toEqual('invalidUserOrPassword')
    })
  })
  it('Login fail', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: Array(51).fill('a').join(''),
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then(res => {
      expect(res.body.error.code).toEqual('invalidUserName')
    })
  })
  it('Invalid user name (empty)', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: '',
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then(res => {
      expect(res.body.error.code).toEqual('invalidUserName')
    })
  })
  it('Invalid user name (too long)', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: Array(500).fill('a').join(''),
      password: 'password'
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then(res => {
      expect(res.body.error.code).toEqual('invalidUserName')
    })
  })
  it('Invalid password (empty)', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: 'asdfasd',
      password: ''
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then(res => {
      expect(res.body.error.code).toEqual('invalidPassword')
    })
  })
  it('Invalid password (too long)', () => {
    return request(app)
    .post('/api/v1/securityToken')
    .send({
      userName: 'asdfasd',
      password: Array(500).fill('a').join('')
    })
    .expect('Content-Type', /json/)
    .expect(400)
    .then(res => {
      expect(res.body.error.code).toEqual('invalidPassword')
    })
  })
})

describe('Profile image endpoint (intended for regression testing)', () => {
  let token = ''

  beforeAll(async () => {
    const result = await request(app)
    .post('/api/v1/securityToken')
    .set('Content-Type', 'application/json')
    .send({
      userName: 'usuario',
      password: 'password'
    })
    token = result.body.token
  })

  it('Retrieve default profile image', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default.png`
    return request(app)
    .post('/api/v1/profile/images')
    .set('Authorization', token)
    .expect('Content-Type', 'image/png')
    .expect(200)
    .then(async res => {
      const buffer = res.body
      const receivedImage = await Jimp.read(buffer)
      
      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)
      
      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve default profile image + country flag', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default-flag.png`
    return request(app)
    .post('/api/v1/profile/images?countryFlag=true')
    .set('Authorization', token)
    .expect('Content-Type', 'image/png')
    .expect(200)
    .then(async res => {
      const buffer = res.body
      const receivedImage = await Jimp.read(buffer)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve default profile image + colored text', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default-flag.png`
    return request(app)
    .post('/api/v1/profile/images?textColor=bf360c')
    .set('Authorization', token)
    .expect('Content-Type', 'image/png')
    .expect(200)
    .then(async res => {
      const buffer = res.body
      const receivedImage = await Jimp.read(buffer)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })
})

describe('Profile image endpoint parameters validation', () => {
  let token = ''
  beforeAll(async () => {
    const result = await request(app)
    .post('/api/v1/securityToken')
    .set('Content-Type', 'application/json')
    .send({
      userName: 'usuario',
      password: 'password'
    })
    token = result.body.token
  })

  it('Unauthorized request', () => {
    return request(app)
    .post('/api/v1/profile/images')
    .expect(401)
  })

  it('Invalid color code', () => {
    return request(app)
    .post('/api/v1/profile/images?textColor=xxxx')
    .set('Authorization', token)
    .expect(400)
  })

  it('Invalid coutryFlag', () => {
    return request(app)
    .post('/api/v1/profile/images?countryFlag=trufa')
    .set('Authorization', token)
    .expect(400)
  })

  it('Invalid saveFile', () => {
    return request(app)
    .post('/api/v1/profile/images?saveFile=trufa')
    .set('Authorization', token)
    .expect(400)
  })
})

describe('Profile image endpoint saving file (intended for regression testing)', () => {
  let token = ''

  beforeAll(async () => {
    const result = await request(app)
    .post('/api/v1/securityToken')
    .set('Content-Type', 'application/json')
    .send({
      userName: 'usuario',
      password: 'password'
    })
    token = result.body.token
  })

  it('Retrieve default profile image', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default-saveFile.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true')
    .set('Authorization', token)
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)
      
      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)
      
      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve default profile image + country flag', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default-saveFile-countryFlag.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true&countryFlag=true')
    .set('Authorization', token)
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve default profile image + colored text', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/default-save-File-textColor.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true&textColor=bf360c')
    .set('Authorization', token)
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })
})

describe('POST /profileImage', () => {
  let token = ''

  beforeAll(async () => {
    const result = await request(app)
    .post('/api/v1/securityToken')
    .set('Content-Type', 'application/json')
    .send({
      userName: 'usuario',
      password: 'password'
    })
    token = result.body.token
  })

  it('Retrieve image from url', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/custom-saveFile.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true')
    .set('Authorization', token)
    .set('Content-Type', 'multipart/form-data')
    .attach('image', './test/background-300.png')
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)
      
      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)
      
      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve image from url + country flag', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/custom-saveFile-countryFlag.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true&countryFlag=true')
    .set('Authorization', token)
    .set('Content-Type', 'multipart/form-data')
    .attach('image', './test/background-300.png')
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })

  it('Retrieve image from url + colored text', () => {
    const expectedImagePath = `${TEST_IMAGES_DIR}/custom-saveFile-textColor.png`
    return request(app)
    .post('/api/v1/profile/images?saveFile=true&textColor=bf360c')
    .set('Authorization', token)
    .set('Content-Type', 'multipart/form-data')
    .attach('image', './test/background-300.png')
    .expect('Content-Type', /json/)
    .expect(200)
    .then(async res => {
      const uri = res.body.uri
      const receivedImage = await Jimp.read(uri)

      // saves image for future tests
      if(!fs.existsSync(expectedImagePath))
        await receivedImage.writeAsync(expectedImagePath)

      const expectedImage = await Jimp.read(expectedImagePath)
      const diff = await Jimp.diff(expectedImage, receivedImage)
      const distance = await Jimp.distance(expectedImage, receivedImage)
      expect(diff.percent).toBeLessThan(.1)
      expect(distance).toBeLessThan(.1)
    })
  })
})