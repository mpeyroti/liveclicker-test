import AWS from 'aws-sdk'
import {v1} from 'uuid'
import { ApplicationError } from 'common';

const BUCKET_NAME = process.env.BUCKET_NAME || 'liveclicker-test/public'
const FILE_EXPIRATION_SECONDS = +(process.env.FILE_EXPIRATION_SECONDS || 86400)
export const S3Store = {
  putObject: async (buffer: Buffer) => {
    try{
      const s3 = new AWS.S3({apiVersion: '2006-03-01'})
      const name = v1() + '.png'
      const resutl = await s3.putObject({
        Key: name,
        Bucket: BUCKET_NAME,
        Body: buffer,
        Expires: new Date(Date.now() + FILE_EXPIRATION_SECONDS * 1000)
      }).promise()
      return {
        uri: `https://liveclicker-test.s3.amazonaws.com/public/${name}`
      }
    }catch(err){
      console.error(err)
      throw new ApplicationError('Error saving image file.')
    }
  }
}