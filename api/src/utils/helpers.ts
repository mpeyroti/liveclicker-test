import { ServerResponse } from "http";

export const writeResponse = (res: ServerResponse, payload: object | string | number, code?: number, mimeType?: string) => {
  res.statusCode = code || 200
  if(mimeType){
    res.setHeader('Content-Type', mimeType)
  }
  else if(typeof payload == 'object'){
    res.setHeader('Content-Type', 'application/json')
  }
  if(typeof payload)
  res.end(typeof payload == "object" ? JSON.stringify(payload): payload)
}

