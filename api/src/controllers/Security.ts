import {Controller, SecurityApi, Credential, AuthenticationResponse, AuthenticatedIncommingMessage, UserInfo, ErrorResponse} from "common";
import {Security as Model, Users, TokenData} from '../models'
import { writeResponse } from "../utils/helpers";
import { SwaggerSecurityHandler } from "swagger-tools";

export const Security: Controller<SecurityApi> = {
  securityTokenPost: async (req, res) => {
    const {credentials} = req.swagger.params
    if(credentials){
      const cred: Credential = credentials.value
      try{
        const result = await Model.getSecurityToken(cred)
        writeResponse(res, result)
      }catch(err){
        if(err.result){
          const response = (err as AuthenticationResponse)
          switch (response.result) {
            case AuthenticationResponse.ResultEnum.InvalidPassword:
            case AuthenticationResponse.ResultEnum.InvalidUserName:
              writeResponse(res, {error: {code: response.result}} as ErrorResponse, 400)
              break;
            case AuthenticationResponse.ResultEnum.InvalidUserOrPassword:
              writeResponse(res, err, 401)
              break;
              
            default:
              writeResponse(res, err, 500)
              break;
          }
        }else{
          writeResponse(res, err, 500)
        }
      }
    }
    res.end(JSON.stringify({status: 200}))
  }
}

export const authenticationHandler: SwaggerSecurityHandler = (request, definition, scope, callback) => {
  const sendError = (code: number, message: string) => (request as any).res.status(code).json({error:{message}})
  const token = request.headers.authorization
  if(token){
    const decodedToken = Model.verifyToken(token)
    if(decodedToken){
      const parsedToken = JSON.parse(decodedToken.data) as TokenData
      // takes data from de data data store instead from token
      const userInfo = Users.find(u => u.id == parsedToken.id)
      if(userInfo){
        (request as AuthenticatedIncommingMessage).userInfo = userInfo
        return callback()
      }else{
        callback(sendError(401, 'Invalid token.'))
      }
    }
  }else{
    return callback(sendError(401, 'This service requires an authorization token.'))
  }
}