import { Controller, ProfileApi, ErrorResponse, AuthenticatedSwaggerRequest, ImageInfo, ApplicationError, ArgumentError} from 'common'
import Jimp from 'jimp'
import { writeResponse } from 'utils/helpers';
import {Profile as ProfileModel} from '../models'

export const Profile: Controller<ProfileApi> = {
  profileImagesPost: async (req, res) => {    
    //TODO enforce max image size limitation
    const {image, textSize = {}, countryFlag = {}, textColor = {}, saveFile = {}} = req.swagger.params
    const buffer: Buffer = image.value ? image.value.buffer: undefined

    const userInfo = (req as AuthenticatedSwaggerRequest).userInfo
    if(userInfo){
      try{
        const result = await ProfileModel.customizeImage(buffer, 
          userInfo, 
          textSize.value, 
          countryFlag.value, 
          textColor.value, 
          saveFile.value)
        res.setHeader('Content-Type', saveFile.value ? 'application/json': 'image/png')
        if(result instanceof Buffer){
          res.end(result, 'binary')
        }else{
          res.end(JSON.stringify(result), 'application/json')
        }
      }catch(err){
        console.error(err)
        if(err instanceof ApplicationError || err instanceof ArgumentError){
          writeResponse(res, {error: (err && err.message) ? err.message: 'Error processing file.'}, 400)
        }else{
          writeResponse(res, {error: (err && err.message) ? err.message: 'Error processing file.'}, err.code || 500)
        }
      }
    }
  }
}