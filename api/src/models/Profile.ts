import {ImageInfo, UserInfo, ProfileApi, ApplicationError, ArgumentError} from "common";
import Jimp from 'jimp'
import {Font, Print} from "@jimp/plugin-print";
import { throws } from "assert";
import { S3Store } from "utils/S3Store";

// This type include all keys of Print.constants which corresponds to font paths, like FONT_SANS_10_BLACK,
// and exludes everything else
// See: https://www.npmjs.com/package/jimp#writing-text
type FontKeys = Exclude<keyof Print['constants'], 'measureText' | 'measureTextHeight' | 'loadFont' | 'loadFont'>
const fontSizes: Record<string, FontKeys> = {
  small: 'FONT_SANS_32_BLACK',
  medium: 'FONT_SANS_64_BLACK',
  big: 'FONT_SANS_128_BLACK'
}
const MARGIN = 10

type t = Exclude<Parameters<ProfileApi['profileImagesPost']>, 'options' | 'image'>

export class Profile {
  static customizeImage = async (origImageBuffer: Buffer, userInfo: UserInfo, ...customization: t): Promise<ImageInfo | Buffer> => {
    let customImage: Jimp
    if(origImageBuffer){
      customImage = await Jimp.read(origImageBuffer)
    }
    else{
      customImage = await Jimp.read('./assets/profile-default.png')
    }
    
    customImage.scaleToFit(256, 256)

    if(customImage){
      try{
        const [textSize, countryFlag, textColor, saveFile] = customization
        let font: Font

        if(textSize && textSize){
          // set default size when the given size is not valid
          let validTextSize: keyof typeof fontSizes = Object.keys(fontSizes).indexOf(textSize) == -1 ? 'medium': textSize
          font = await Jimp.loadFont(Jimp[fontSizes[validTextSize]])
        }else{
          font = await Jimp.loadFont(Jimp[fontSizes['medium']])
        }

        if(userInfo){
          const textLine1 = userInfo.firstName
          const textLine2 = userInfo.lastName
          
          const textWidthLine1 = Jimp.measureText(font, textLine1)
          const textWidthLine2 = Jimp.measureText(font, textLine2)
          const textWidth = Math.max(Jimp.measureText(font, textLine1), Jimp.measureText(font, textLine2))
          
          const textHeightLine1 = Jimp.measureTextHeight(font, textLine1, 1000)
          const textHeightLine2 = Jimp.measureTextHeight(font, textLine2, 1000)
          const textHeight = textHeightLine1 + textHeightLine2
          const textImage = new Jimp(textWidth, textHeight, 0x00000000)
          
          textImage.print(font, textWidth - textWidthLine1, 10, textLine1)
          textImage.print(font, textWidth - textWidthLine2, textHeightLine1, textLine2)
          
          if(textColor){
            if(/[0-9a-f]{6}/.test(textColor.toLowerCase())){
              textImage.color([
                {apply: 'xor', params:[textColor]}
              ])
            }else{
              throw new ArgumentError('Color value has an invalid format.', 'textColor')
            }
          }

          const customizedImageWidth = customImage.getWidth()
          const customizedImageHeight = customImage.getHeight()

          textImage.scaleToFit(customizedImageWidth/2, customizedImageHeight)

          const x = customizedImageWidth - textImage.getWidth() - MARGIN
          const y = customizedImageHeight - textImage.getHeight() - MARGIN
          customImage.composite(textImage, x, y)      

          if(countryFlag && userInfo.countryCode){
            const flagImage = await Jimp.read(`https://www.countryflags.io/${userInfo.countryCode}/flat/64.png`, )
            customImage.composite(flagImage, 5, 5)
          }
        }else{
          throw new Error('User data is not available.')
        }

        const customImageBuffer = await customImage.getBufferAsync(Jimp.MIME_PNG)
        if(saveFile){
          const result = await S3Store.putObject(customImageBuffer)
          return {
            uri: result.uri
          } as ImageInfo
        }else{
          return customImageBuffer
        }

      }catch(err){
        console.error(err)
        if(err instanceof ApplicationError || err instanceof ArgumentError)
          throw err
        else
          throw new ApplicationError('Error processing image.')
      }  
    }else{
      throw new ApplicationError('Background image not available')
    }
  }
}