import {Credential, AuthenticationResponse, UserInfo} from 'common'
import {sign, verify} from 'jsonwebtoken'
import { SwaggerSecurityHandler } from 'swagger-tools';

export type TokenData = UserInfo & {password: string, id: number}
export const Users: TokenData[] = [
  {id: 1, userName: 'usuario', password: 'password', firstName: 'Matias', lastName: 'Peyroti', countryCode: 'UY'}
]

const JWT_SECRET = process.env.JWT_SECRET || 'c5IzKG1mVLqJYlZ6Joni'
const JWT_EXPIRATION = process.env.JWT_EXPIRATION || "7d"

export class Security{
  static getSecurityToken = async (credential: Credential): Promise<AuthenticationResponse> => {
    //TODO: make something prittier
    return new Promise<AuthenticationResponse>(async (resolve, reject) => {
      if(!credential.userName || credential.userName.length > 50){
        reject({
          result: AuthenticationResponse.ResultEnum.InvalidUserName
        })
      }
      if(!credential.password || credential.password.length > 50){
        reject({
          result: AuthenticationResponse.ResultEnum.InvalidPassword
        })
      }
      const found = Users.find((c) => c.userName == credential.userName.toLowerCase() && c.password == credential.password)
      if(found){
        const {password, ...rest} = found
        const data = JSON.stringify(rest)
        try{
          const token = sign({data}, JWT_SECRET, {
            expiresIn: JWT_EXPIRATION
          })
          resolve({
            result: AuthenticationResponse.ResultEnum.Success,
            token
          })
        }catch(err){
          console.error(err)
          reject({
            result: "UnknownServerError"
          })
        }
      }
      else{
        reject({
          result: AuthenticationResponse.ResultEnum.InvalidUserOrPassword
        })
      }
    })
  }
  static verifyToken = (token: string): DecodedToken | undefined => {
    try{
      // decoded.data contains the payload
      let decoded = verify(token, JWT_SECRET)
      return decoded as DecodedToken
    }catch(err){
      console.error(`ferify error: ${JSON.stringify(err)}`)
      return undefined
    }
  }
}

type DecodedToken = {data: string, iat: number, exp: number}