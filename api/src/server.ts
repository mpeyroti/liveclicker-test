import express from 'express'
import http from 'http'
import {initializeMiddleware} from 'swagger-tools'
import * as Controllers from './controllers'
import {safeLoad} from 'js-yaml'
import { overrideConsoleMethods } from 'utils/logger';
import * as fs from 'fs'
import * as path from 'path'

const PORT = process.env.PORT || 3001
const specFile = fs.readFileSync(path.resolve(__dirname, 'openapi.yaml'), 'utf8')
const spec = safeLoad(specFile)

spec.basePath = process.env.BASE_PATH || spec.baseUrl

export const app = express()
export const httpServer = http.createServer(app)

overrideConsoleMethods()

const controllersObject = Object.keys(Controllers).reduce((map, key) => {
  let controller = Controllers[key as keyof typeof Controllers]
  Object.keys(controller).forEach(funcKey => {
    const element = controller[funcKey as keyof typeof controller]
    if(typeof element == 'function')
      map[`${key}_${funcKey}`] = element})
  return map
}, {} as any)

initializeMiddleware(spec, (middleware) => {
  // This is the base middleware that will analyze a request route, 
  // match it to an API in your Swagger document(s) and then annotate the request, 
  // using req.swagger, with the pertinent details.
  app.use(middleware.swaggerMetadata())
  
  // This middleware allows you to wire up authentication/authorization handlers
  // based on the definitions in your Swagger document(s).
  app.use(middleware.swaggerSecurity({
    AuthHeader: Controllers.authenticationHandler
  }))
  
  // This middleware will validate your request/responses based on the operations
  // in your Swagger document(s).
  app.use(middleware.swaggerValidator({
    validateResponse: false
  }))
  
  // This middleware allows you to wire up request handlers
  // based on the operation definitions in your Swagger document(s).
  app.use(middleware.swaggerRouter({
    controllers: controllersObject
  }))

})

httpServer.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})