import { BaseAPI } from './generated';
import { SwaggerRouter20HandlerFunction, Swagger20Request } from 'swagger-tools';
import { IncomingMessage } from 'http';

export * from './generated'

export type Controller<T extends BaseAPI> = Partial<Record<Extract<keyof T, string>, SwaggerRouter20HandlerFunction>>

export interface UserInfo{
  userName: string
  firstName: string
  lastName: string
  countryCode: string
}

export declare type AuthenticatedIncommingMessage = IncomingMessage & {
  userInfo?: UserInfo;
};

export declare type AuthenticatedSwaggerRequest = Swagger20Request<any> & {
  userInfo?: UserInfo;
};

export class ApplicationError extends Error{
  constructor(message: string){
    super(message)
    Object.setPrototypeOf(this, ApplicationError.prototype)
  }
}

export class ArgumentError extends Error{
  private _argument: string
  
  public get argument() : string {
    return this._argument
  }
  
  constructor(message: string, argument: string){
    super(message)
    Object.setPrototypeOf(this, ArgumentError.prototype)
    this._argument = argument
  }
}
